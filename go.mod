module github.com/gookit/gcli/v3

go 1.13

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gookit/color v1.5.0
	github.com/gookit/goutil v0.5.1
	github.com/stretchr/testify v1.7.1
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83
	golang.org/x/term v0.0.0-20210220032956-6a3ed077a48d // indirect
)

// for develop
//replace github.com/gookit/goutil => ../goutil
